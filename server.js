var express = require('express');
var morgan = require('morgan');
var parser = require('body-parser');
var path = require('path');
var Twitter = require('twitter');
var cors = require('cors');

var app = express();
app.use(express.static(path.join(__dirname, 'build')));
app.use(morgan('dev'));
app.use(parser.json());
app.use(parser.urlencoded({ extended: true }));
app.use(cors());
app.set('port', (process.env.PORT || 5000));

var twitter = new Twitter({
 consumer_key: 'eBT0nXbZXWRTkw4iMc305kFyk',
 consumer_secret: 'zpVRbLKTf0FRKspHLFt8MSWJYkb2FR5pCXNxubAbyLjsdHGYGh',
 access_token_key: '936336016037810176-kJqfS2intDVz7hVIq7Lfi6yiYLUVTvL',
 access_token_secret: 'OAdLg8OHQqELWr8J2vYJbIUzqEOu96ftGZFpPnVVWI2u4'
});

app.use( (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/build/index.html');
});

app.options('/tweet', (req, res) => {
  res.end();
});

app.post('/tweet', (req, res) => {
  twitter.post(
    'statuses/update',
    { status: req.body.text },
    (error, tweet, response) => {
      if(error) throw error;
    });
  res.end('done!');
});

app.listen(app.get('port'), (err) => {
  err ? console.log(err) : console.log('Connected on port', app.get('port'));
});
  