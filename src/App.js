import React, { Component } from 'react';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tweet: ''
    };
  }

  onChange = (e) => {
    this.setState({
      tweet: e.target.value
    });
  }

  tweetIt = (input) => {
    if (this.state.tweet === '') {
      alert('Nothing to tweet!');
      return;
    }
    axios.post(
      '/tweet',
      {text: this.state.tweet},
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
          "Access-Control-Allow-Headers": "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
        }
      }
    ).then((res) => {
      alert('Tweet successfully sent!');
    }).catch(console.log);
    document.getElementById("tweetInput").value = '';
  }

  render() {
    return (
      <div className="App container">
        <img src='https://500.co/wp-content/uploads/2016/04/cashbasha_icon_transparent2-300x300.jpg' className="App-logo" alt="logo" />
        <p className="App-intro">
          Please enter the text of your tweet below
        </p>
        <div className="row center-block">
          <div className="col-md-6 center-block">
            <div className="input-group center-block">
              <span className="input-group-btn center-block">
                <input className="form-control" placeholder="Tweet Here..." id="tweetInput" onChange={this.onChange}/>
                <button className="btn btn-success" onClick={this.tweetIt}>Tweet!</button>
              </span>
            </div>
          </div>
        </div>
        <div>
          <footer className="footer">
            <p className="lead"> Done by Siraj Kakeh</p>
            <p className="lead"> sirajonline.89@gmail.com</p>
          </footer>
        </div>
      </div>
    );
  }
}

export default App;
